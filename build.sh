#!/usr/bin/env bash

set -e

vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
VERSION="${vers#v}"
DATE="$(date +%FT%T)"
REV="$(git log -1 --pretty=format:%h%n)"

docker build \
       --label=org.label-schema.version=${VERSION} \
       --label=org.label-schema.build-date=${DATE} \
       --label=org.label-schema.vcs-ref=${REV} \
       -t rmarver/apf11builder_gcc5_4:${VERSION} -t rmarver/apf11builder_gcc5_4:latest .
