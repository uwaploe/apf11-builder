# Docker image to provide a GCC cross-compiler for the APF-11 software
FROM debian:stable-slim
LABEL org.opencontainers.image.authors="ram16@uw.edu"

RUN dpkg --add-architecture i386 && \
    apt-get update && apt-get -y --no-install-recommends install \
    build-essential \
    curl \
    bzip2 \
    make \
    git \
    cmake \
    ca-certificates \
    xz-utils \
    libc6-i386

ENV GCC_VERS=5_4-2016q3 \
    BUILD_DATE=20160926 \
    GCC_MAJOR=5.0 \
    GCC_DIR=5-2016-q3-update
LABEL gcc_version="${GCC_VERS}"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN curl -sS -L "https://launchpad.net/gcc-arm-embedded/${GCC_MAJOR}/${GCC_DIR}/+download/gcc-arm-none-eabi-${GCC_VERS}-${BUILD_DATE}-linux.tar.bz2" | tar -x -j -f -

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

ENV PATH=/gcc-arm-none-eabi-${GCC_VERS}/bin:$PATH
